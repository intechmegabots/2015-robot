/*
Copyright 2015 Intech Megabots

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "PWMInput.h"
#include <vector>
#include <algorithm>
#include <iostream>

#define BUFFER_SIZE 64

template <class T>
T getMedian(RotoBuffer<T>& b, int count)
{
  std::vector<T> r(count);
  for(int i=0;i<count;i++)
    r[i]=b.at(i);
  std::sort(r.begin(),r.end());
  return r[count/2];
}

void PWMInputMisc::InterruptHandlerRising(unsigned int a, void *data)
{
  PWMInputMisc::InterruptParams* params = (PWMInputMisc::InterruptParams*)data;
  params->startPoint = std::chrono::high_resolution_clock::now();
}

void PWMInputMisc::InterruptHandlerFalling(unsigned int a, void *data)
{
  PWMInputMisc::InterruptParams* params = (PWMInputMisc::InterruptParams*)data;
  params->duration = (std::chrono::duration_cast<std::chrono::microseconds>
    (std::chrono::high_resolution_clock::now()-params->startPoint).count()-90.0)/1113.0;
  if(params->duration>1) params->duration-=1;
  if(params->duration<0) params->duration+=1;
  params->rb->push({params->startPoint,params->duration});
}

PWMInput::PWMInput(uint32_t channel, uint32_t channel2) : buf(BUFFER_SIZE), deltaBuf(5)
{
  this->inputRising = new DigitalInput(channel);
  this->inputFalling = new DigitalInput(channel2);
  this->data.duration=-1;
  this->data.rb=&buf;
  inputRising->RequestInterrupts(PWMInputMisc::InterruptHandlerRising,&this->data);
  inputFalling->RequestInterrupts(PWMInputMisc::InterruptHandlerFalling,&this->data);
  inputRising->SetUpSourceEdge(true,false);
  inputFalling->SetUpSourceEdge(false,true);
  inputRising->EnableInterrupts();
  inputFalling->EnableInterrupts();
}

double PWMInput::durDelt(int a, int b){
  return buf.at(a).duration-buf.at(b).duration;
}
double PWMInput::PIDGet()
{
  return getValue()/800;
}

#define BUF_SAMPLE 10

double PWMInput::getValue()
{
  std::vector<double> a(BUF_SAMPLE);
  double delta;
  for(int i=0;i<BUF_SAMPLE;i++)
  {
    a[i]=this->buf.at(i).duration;
  }
  std::sort(a.begin(),a.end(),[](double a, double b){
    return a<b;
  });
  delta = a[BUF_SAMPLE/2];
  for(int i=0;i<BUF_SAMPLE;i++)
    a[i]=this->buf.at(BUF_SAMPLE+i).duration;
  std::sort(a.begin(),a.end(),[](double a, double b){
    return a<b;
  });
  delta-=a[BUF_SAMPLE/2];
  deltaBuf.push(delta/(.0001217*BUF_SAMPLE));
  return getMedian(deltaBuf,5); // return rpm .0001217 = minutes to complete a encoder update
}

PWMInput::~PWMInput()
{
  delete this->inputRising;
  delete this->inputFalling;
}


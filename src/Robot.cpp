/*
Copyright 2015 Intech Megabots

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "fixit.hpp"
#include "autonframework.h"
#include "drivetrain.h"
#include "lifter.h"
#include "joystickdefs.hpp"

#include <cmath>
#include "PWMInput.h"

double dabs(double d);

void nullButMax(float* v, int l)
{
  for(int i=0;i<l;i++)
  for(int k=0;k<l;k++)
  {
    if(i==k) continue;
    if(dabs(v[i])>=dabs(v[k])) v[k]=0;
  }
}


Joystick *RightJoystick;
Joystick *LeftJoystick;

class Robot: public IterativeRobot
{
private:

  Drivetrain *drivetrain;

  Lifter *lifter;
  bool clawValue;
  bool clawToggleCooldown;

  Compressor compressor;

  AutoStuff *autoStuff; // descriptive!

  bool compressorToggleCooldown = false;
  bool compressorToggleThing = true;

  float joysOff;

public:
  Robot():
    compressor(11)
  {
  }

  void RobotInit()
  {
    RightJoystick = new Joystick(0);
    LeftJoystick  = new Joystick(1);
    drivetrain = new Drivetrain();
    lifter = new Lifter();
    autoStuff = new AutoStuff(drivetrain,lifter,LiveWindow::GetInstance());
    SmartDashboard::init();
    autoStuff->setDefaultAuton(1);
    clawValue = false;
    clawToggleCooldown = false;

   // compressor.Stop();
  }

// drive into the Auto zone and stop.
// pick up one tote and move one can into the Auto zone, place the tote and stop.
// pick up one tote, hit/move around the first can. Pick up second tote and move second can into Auto zone, place the stack, and stop.
// Pick up one tote, pick up the second tote, pick up the third tote, push the third can into the Auto zone, place the stack, and stop.

  double getTotalValues()
  {
    return dabs(DriveX) + dabs(DriveY) + dabs(DriveR) + dabs(LiftVertical);
  }

  void TeleopInit()
  {
    joysOff = 1.0;
    drivetrain->Enable();
    lifter->Enable();
  }
  void TeleopPeriodic()
  {
    // fix disable-all-motors-on bug
    if (joysOff>0.1 || joysOff<-0.1)
    {
      joysOff = getTotalValues();
      return;
    }

    // Claw input handling
    if(ClawToggle)
    {
      if(!clawToggleCooldown)
      {
        clawToggleCooldown = true;
        clawValue^=1;
      }
    }
    else
      clawToggleCooldown = false;
    lifter->setClaw(clawValue^ClawLeftMoment,clawValue^ClawRightMoment);

    //update lifter
    lifter->ManualControl(LiftVertical,LiftFaster);
    lifter->update();

    if (LeftJoystick->GetRawButton(5))
    {
      lifter->gotoTarget(.25);
    }
    else if (LeftJoystick->GetRawButton(6))
    {
      lifter->gotoTarget(.5);
    }
    else if (LeftJoystick->GetRawButton(7))
    {
      lifter->gotoTarget(.75);
    }

    float axis[3] = {
      DriveX, DriveY, DriveR
    };
    if(DriveIgnoreAxis)
      nullButMax(axis,3);
    drivetrain->update(axis[0],axis[1],axis[2],Throttle);

    // allow the compressor to be toggled
    if(CompressorToggle)
    {
      if(!compressorToggleCooldown)
      {
        std::cout << "Compressor Toggle" << std::endl;
        if(compressorToggleThing)
        {
          compressor.Stop();
          compressorToggleThing=false;
        }
        else
        {
          compressor.Start();
          compressorToggleThing=true;
        }
        compressorToggleCooldown=true;
      }
    }
    else
    {
      compressorToggleCooldown=false;
    }
  }

  void AutonomousInit()
  {// handoff to the autonhandler
    autoStuff->onInit();
  }
  void AutonomousPeriodic()
  {
    autoStuff->onPeriodic();
  }

  void TestPeriodic() {}
  void TestInit() {}
};


double dabs(double d)
{
  if(d<0)return -d;
  return d;
}


START_ROBOT_CLASS(Robot);

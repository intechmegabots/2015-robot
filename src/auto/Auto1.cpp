#include "Auto1.hpp"

Auto1::Auto1(Drivetrain *drive, Lifter *lifter)
{
  this->drivetrain=drive;
  this->lifter=lifter;
}
void Auto1::init()
{
  drivetrain->Enable();
  lifter->Enable();
  std::cout << "Starting auton 1" << std::endl;
}
void Auto1::Periodic(double autotime)
{

  if (autotime < 5)
  {
    lifter->update();
    drivetrain->update(0,-1,0,.5);
    lifter->setClaw(0,0);
  }
  else if (autotime < 5.5)
  {
    lifter->setClaw(1,1);
    drivetrain->update(0,1,0,.25);
  }
  else
  {
    drivetrain->update(0,0,0,0);
  }
 /* if (autotime < .5) lifter->setClaw(0,0);
  lifter->update();
  if(autotime > .5 && autotime < 4) {
    lifter->gotoTarget(.25);
    if (autotime > .75 && autotime < 2)
    {
      drivetrain->update(0,1,0,.75);
    }
    if (autotime > 2 && autotime < 2.5)
    {
      drivetrain->update(-1,0,0,.75);
    }
    if (autotime > 2.5 && autotime < 3)
    {
      drivetrain->update(0,-1,0,.75);
    }
    if (autotime > 3 && autotime < 4)
    {
      drivetrain->update(1,0,0,.75);
    }
  }
  if(autotime > 4)
  {
    lifter->ManualControl(-.25,0);
  }
  if(autotime > 4.5 && autotime < 4.9)
  {
    lifter->setClaw(1,1);
    drivetrain->update(0,-1,0,.25);
  }
  if (autotime > 4.9)
  {
    drivetrain->update(0,0,0,0);
  }*/
}
Auto1::~Auto1()
{

}

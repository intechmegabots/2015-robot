#ifndef AUTO_1_GUARD
#define AUTO_1_GUARD

#include "../drivetrain.h"
#include "../lifter.h"
#include "../autonFramework.h"

class Auto1: public Autonomous{
public:
  Auto1(Drivetrain *drive, Lifter *lifter);
  void init();
  void Periodic(double time);
  ~Auto1();
private:
  Drivetrain *drivetrain;
  Lifter *lifter;
};

#endif //AUTO_1_GUARD

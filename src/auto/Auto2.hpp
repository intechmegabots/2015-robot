#ifndef AUTO_2_GUARD
#define AUTO_2_GUARD

#include "../drivetrain.h"
#include "../lifter.h"
#include "../autonFramework.h"

class Auto2: public Autonomous{
public:
  Auto2(Drivetrain *drive, Lifter *lifter);
  void init();
  void Periodic(double time);
  ~Auto2();
private:
  bool toteClose; //true if there is a tote within picking up range, false if not
  float toteCenterError; //center of the tote - the center of the image
  Drivetrain* drivetrain;
  Lifter* lifter;
};

#endif //AUTO_2_GUARD

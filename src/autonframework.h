#ifndef AUTONFRAMEWORK_H
#define AUTONFRAMEWORK_H

#include "drivetrain.h"
#include "lifter.h"
#include "fixit.hpp"

class Autonomous
{
public:
  //virtual Autonomous(Drivetrain &drive, Lifter &lifter);
  virtual void init()=0;
  virtual void Periodic(double time)=0;
  virtual ~Autonomous();
};

class AutoStuff
{
public:
  AutoStuff(Drivetrain *drive, Lifter *lifter, LiveWindow *liveWindow);
  void onPeriodic();
  void onInit();
  void setDefaultAuton(int autonIndex);
  int getDefaultAuton();
private:
  void setAuton(int autonIndex);
  Drivetrain* drive;
  Lifter* lifter;
  time_t start;
  int defaultAuton;
  Autonomous* currentAuton = NULL;
  SendableChooser autonSelector;
  LiveWindow* liveWindow;
};


#endif // AUTONFRAMEWORK_H


#ifndef PIDCONTROLLER_H
#define PIDCONTROLLER_H

#include "WPIlib.h"
#include "PWMInput.h"

class PIDDY
{
public:
  PIDDY(double p, double i, double d, double iMax);
  void Set(double p, double i, double d, double iMax);
  double update(double target, double sensor);
  ~PIDDY();
private:
  double p;
  double i;
  double d;
  double iMax;
  double integral;
  double previousError;
};

class DriveTalon : public CANTalon
{
public:
  DriveTalon(int id);
  //using CANTalon::CANTalon; // inherit constructor
  void setInfo(double p, double i, double d, double iMax, int DIOa, int DIOb);
  void Set(double target);
private:
  PIDDY* PIDdy;
  PWMInput* sensor;
  double target;
  int id;
};


#endif // PIDCONTROLLER_H
